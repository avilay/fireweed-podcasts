from urllib.parse import urlparse

import dateutil.parser as dtparser

import feedparser as fp
from podcasts.models import Episode, Podcast


class InvalidUrl(Exception):
    pass


class InvalidFeed(Exception):
    pass


class InvalidPodcast(Exception):
    pass


def parse(feed_url):
    if not feed_url:
            raise InvalidUrl(f'{feed_url} is not a valid url')

    url = urlparse(feed_url)
    if not url.scheme or not url.netloc:
        raise InvalidUrl(f'{feed_url} is not a valid url')

    feed = fp.parse(feed_url)
    if 'bozo_exception' in feed:
        raise InvalidFeed(str(feed.bozo_exception))

    if 'enclosures' not in feed.entries[0]:
        raise InvalidPodcast(f'{feed_url} does not seem to have any media')

    if not feed.entries[0]['enclosures'][0]['type'].startswith('audio'):
        raise InvalidPodcast(f'{feed_url} does not seem to have any audio')

    title = feed.feed.get('title')
    link = feed.feed.get('link')
    if feed.feed.get('summary'):
        description = feed.feed.get('summary')
    else:
        description = feed.feed.get('subtitle')
    if feed.feed.get('author'):
        author = feed.feed.get('author')
    else:
        author = feed.feed.get('credit')
    image_url = feed.feed.get('image')
    if image_url:
        image_url = image_url['href']
    published_on = feed.feed.get('published')
    if published_on:
        published_on = dtparser.parse(published_on)

    episodes = []
    for entry in feed.entries:
        guid = entry.id
        entry_published_on = entry.get('published')
        if entry_published_on:
            entry_published_on = dtparser.parse(entry_published_on)
        entry_title = entry.get('title')
        entry_desc = entry.get('summary')
        entry_image_url = entry.get('image')
        if entry_image_url:
            entry_image_url = entry_image_url['href']
        entry_link = entry.get('link')
        podcast_url = None
        enclosures = entry.get('enclosures')
        if enclosures:
            podcast_url = entry.enclosures[0]['href']
        episode = Episode(
            guid=guid,
            title=entry_title,
            published_on=entry_published_on,
            description=entry_desc,
            podcast_url=podcast_url,
            image_url=entry_image_url,
            web_url=entry_link,
        )
        episodes.append(episode)

    podcast = Podcast(
        link=link,
        title=title,
        description=description,
        author=author,
        image_url=image_url,
        feed_url=feed_url,
        published_on=published_on,
    )
    return podcast, episodes
