import logging
import os

import requests
from flask import Flask, abort, jsonify, request
from flask.logging import default_handler

from podcasts.models import EntityNotFoundError, PgDatastore
from podcasts.parser import InvalidFeed, InvalidPodcast, InvalidUrl, parse

from .crawler import crawl

METADATA_URL = 'http://metadata/computeMetadata/v1/project/attributes/podcasts_config'


def config_log():
    logformat = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
    formatter = logging.Formatter(logformat)

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(ch)


def read_config():
    env = os.environ.get('ENV', 'dev')
    app.logger.info(f'Operating in {env} env')
    if env == 'prod':
        resp = requests.get(METADATA_URL, headers={'Metadata-Flavor': 'Google'})
        return resp.json()
    else:
        return {
            'POSTGRES': {
                'host': 'localhost',
                'user': 'postgres',
                'database': 'fireweed'
            }
        }


app = Flask(__name__)
config = read_config()
app.logger.removeHandler(default_handler)
config_log()
app.logger.info(f'Config: {config}')
ds = PgDatastore(config)


def get_podcasts():
    all_podcasts = ds.get_all_podcasts()
    return jsonify([pc.to_dict() for pc in all_podcasts])


def post_podcast():
    req = request.get_json()
    feed_url = req['feed_url']
    podcast = ds.get_podcast_by_feed_url(feed_url)
    if podcast is None:
        pc, eps = parse(feed_url)
        podcast = ds.add_podcast(pc)
    return jsonify(podcast.to_dict())


@app.route('/wirecheck')
def wirecheck():
    app.logger.debug('This is debug log')
    app.logger.info('This is info log')
    app.logger.warn('This is warn log')
    app.logger.error('This is error log')
    app.logger.info(request.headers)
    return jsonify({'message': 'wirecheck complete'})


@app.route('/podcasts', methods=['GET', 'POST'])
def podcasts():
    if request.method == 'GET':
        return get_podcasts()
    else:
        return post_podcast()


@app.route('/podcasts/<podcast_id>')
def get_podcast(podcast_id):
    podcast = ds.get_podcast(int(podcast_id))
    return jsonify(podcast.to_dict())


@app.route('/podcasts/<podcast_id>/episodes')
def get_episodes(podcast_id):
    eps = ds.get_episodes(int(podcast_id))
    return jsonify([ep.to_dict() for ep in eps])


@app.route('/podcasts/<podcast_id>/episodes/<episode_id>')
def get_episode(podcast_id, episode_id):
    ep = ds.get_episode(int(podcast_id), int(episode_id))
    return jsonify(ep.to_dict())


@app.route('/crawl')
def get_crawl():
    gae_cron = False
    if 'X-Appengine-Cron' in request.headers:
        gae_cron = request.headers['X-Appengine-cron'] == 'true'
    if not gae_cron:
        abort(401)
    crawl(ds)
    return jsonify({"status": "completed"})


@app.errorhandler(EntityNotFoundError)
@app.errorhandler(ValueError)
@app.errorhandler(InvalidUrl)
@app.errorhandler(InvalidFeed)
@app.errorhandler(InvalidPodcast)
def handle_bad_request(e):
    err = {
        'message': str(e)
    }
    return jsonify(err), 400


@app.errorhandler(401)
def handle_forbidden(e):
    err = {
        'message': 'You are not authorized to run the crawl job!'
    }
    return jsonify(err)
