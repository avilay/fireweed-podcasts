CREATE TABLE IF NOT EXISTS podcasts (
    podcast_id serial NOT NULL CONSTRAINT pd_pri_key PRIMARY KEY,
    link varchar(2048) NOT NULL,
    title varchar(2048) NOT NULL,
    description text,
    author varchar(1024),
    image_url varchar(2048),
    feed_url varchar(2048) NOT NULL,
    published_on timestamptz
);

CREATE TABLE IF NOT EXISTS episodes (
    episode_id serial NOT NULL CONSTRAINT ep_pri_key PRIMARY KEY,
    podcast_id integer NOT NULL CONSTRAINT fk_ep_podcasts REFERENCES podcasts ON DELETE CASCADE,
    guid varchar(1024) NOT NULL,
    title varchar(2048) NOT NULL,
    published_on timestamptz,
    description text,
    podcast_url varchar(2048),
    image_url varchar(2048),
    web_url varchar(2048)
);

CREATE TABLE IF NOT EXISTS crawl_history (
    last_crawl_on timestamptz,
    num_podcasts int,
    num_new_episodes int
);