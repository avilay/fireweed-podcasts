Fireweed Services
=================
First questions first - why this name? I like to name my projects after plants found in the Pacific Northwest. This is one of them, also known as [Chamaenerion angustifolium](https://en.wikipedia.org/wiki/Chamaenerion_angustifolium)

Firweed group of services is a stack of demo micro-services. Each micro-service will have its own git repo called fireweed-{service name}.

The full fireweed app is a podcast playlist manager, where end users can add episodes from across different podcasts into their playlist.


Fireweed Podcasts
=================
At a high level, this micro-service provides a set of APIs that lets the caller add a new podcast feed to the system, query the system for existing podcasts and their episodes, ask the system to crawl for new episodes for all existing podcasts already in the system. This is one "pod" (to use K8 terminology) of micro-services that would typically be deployed together.

![Architecture](arch.png)

It consists of 3 components.

### Fireweed DB
This is the datastore that holds all the podcasts the system knows about, and their episodes. This has been implemented as a GCP managed PostgreSQL RDBMS for no other reason than ease of use. This component is not very relevant to the micro-services part of the demo.

### Crawler
This component gets all the podcast feed URL from the fireweed DB, crawls the feeds and updates the DB with any new episodes. This is a batch job that is supposed to run with some fixed periodicity, typically once a day. When implemented as GAE, the crawler has to be wrapped in an API that only GAE cron jobs can call.

### APIs
This provides a set of REST APIs to get podcasts and specific episodes from a podcast.


Deployment
==========
### Create GCP Project
Create a GCP project and enable the following APIs:
  * Cloud SQL Admin API
  * Google Cloud SQL
  * Cloud Endpoints Portal
  * Google Cloud Endpoints
  * Google App Engine Flexible Environment

### Create GCP PostgreSQL
Create a PostgreSQL instance in GCP and run the `create.sql` script by following the instructions in [GCP docs](https://cloud.google.com/sql/docs/postgres/).
TODO

### Setup PROJECT-ID
Add project specific information to these config files:
  * app.yaml: Grab your SQL instance connection name from GCP and replace {YOUR-GCP-POSTGRES-INSTANCE-CONN-NAME}.
  * swagger.yaml: Get your project ID and replace {YOUR-PROJECT-ID-HERE} in line 6.

### Setup Project Metadata
Create a key called `podcasts_config` and set its value to the following JSON in your project metadat.

```
{
    "POSTGRES": {
        "host": "/cloudsql/<instance conn name>,
        "user": "postgres",
        "password": <password here>,
        "database": "firweed"
    }
}
```
Instructions for how to setup project metadata can be found [here](https://cloud.google.com/compute/docs/storing-retrieving-metadata#projectwide).

### Deploy App
Deploy the Flask app as the `podcast` service.
```
$ cd fireweed-podcasts
$ gcloud app deploy
```

### Deploy Endopint API
```
$ cd fireweed-podcasts
$ gcloud endpoints services deploy swagger.yaml
```

### [Optional] Deploy cron
```
$ cd fireweed-podcasts
$ gcloud app deploy cron.yaml
```
